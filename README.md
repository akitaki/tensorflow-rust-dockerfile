# Rust Tensorflow Dockerfile

Pre-built image available at
[dockerhub](https://hub.docker.com/r/akitaki/tensorflow-rust).

This repository contains a somewhat hacky `Dockerfile` for using Tensorflow from
Rust.  The following things come pre-installed in the image.

* CUDA and cuDNN.
* Latest stable Rust toolchain at the time.
* `sccache`, configured as wrapper for Cargo.
* `libtensorflow.so` (i.e. the C API).
* `pkg-config`, `libssl-dev`, and `gcc`.

Intended usage of the image is like this.

```sh
# Create volume to store sccache local cache files
podman volume create sccache
# Run container
podman run --rm -itd \
    -v sccache:/root/.cache/sccache \
    -v $(pwd):/data/ \
    docker.io/akitaki/tensorflow-rust:2.4.0-gpu
# Spawn shell inside the container
podman exec -it $NAME_OF_CONTAINER bash
```

## Building

```sh
BUILDAH_RUNTIME=crun podman build . --tag docker.io/akitaki/tensorflow-rust:2.4.0-gpu
```
